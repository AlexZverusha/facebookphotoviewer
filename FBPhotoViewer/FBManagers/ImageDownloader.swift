//
//  PhotoDownloader.swift
//  FBPhotoViewer
//
//  Created by Alex on 27.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit


protocol Cancelable {
    func cancel()
}

extension URLSessionDataTask: Cancelable {  }

enum ImageDownloaderError: Error {
    case invalidRequest
    case notFound
    case unknown
    
    init(httpCode: Int) {
        switch httpCode {
        case 404:
            self = .notFound
        case 400:
            self = .invalidRequest
        default:
            self = .unknown
        }
    }
}

typealias ImageDownloaderCompletionHandler = (_ image: UIImage?, _ error: Error?) -> ()

struct ImageDownloader  {
    
    static func download(from url: URL, completion: ImageDownloaderCompletionHandler?) -> Cancelable {
        
        let task = URLSession.shared.dataTask(with: url) { aData, aResponse, anError in
            if anError == nil, let data = aData, let image = UIImage(data: data) {
                completion?(image, nil)
                return
            }
            if let error = anError {
                completion?(nil, error)
                return
            }
            if let response = aResponse as? HTTPURLResponse {
                completion?(nil, ImageDownloaderError(httpCode: response.statusCode))
                return
            }
            completion?(nil, ImageDownloaderError.unknown)
        }
        task.resume()
        return task
    }
}


