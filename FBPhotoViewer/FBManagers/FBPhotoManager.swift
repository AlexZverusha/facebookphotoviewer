//
//  FBPhotoManager.swift
//  FBPhotoViewer
//
//  Created by Alex on 21.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit



class FBPhotoManager {
    
    
    func fetchAlbums(completion: @escaping ([Album]) -> ()) {
        if FBSDKAccessToken.current() != nil {
            guard let userID = FBSDKAccessToken.current().userID else {return}
            let path = "/" + String(describing: userID) + "/albums"
            FBSDKGraphRequest(graphPath: path , parameters: Constant.fetchAlbumsParameters).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    var jsonData: Data?
                    jsonData = try? JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    do {
                        let list = try decoder.decode(Albums.self, from: jsonData!)
                        completion(list.albums)
                    } catch {
                        print(Constant.errorParsingMessage)
                    }
                }
            })
        }
    }
    
    func fetchPhotos(idAlbum: String, completion: @escaping ([Photo]) -> ()) {
        
        if FBSDKAccessToken.current() != nil {
            
            let path = "/" + idAlbum + "/photos"
            FBSDKGraphRequest(graphPath: path , parameters: Constant.fetchPhotosParameters).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    var jsonData: Data?
                    jsonData = try? JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    do {
                        let list = try decoder.decode(Photos.self, from: jsonData!)
                        completion(list.photos)
                    } catch {
                        print(Constant.errorParsingMessage)
                    }
                }
            })
        }
    }
}

