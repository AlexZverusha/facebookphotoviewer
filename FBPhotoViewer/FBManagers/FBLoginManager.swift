//
//  FBLoginManager.swift
//  FBPhotoViewer
//
//  Created by Alex on 21.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit



class FBLoginManager {
    
    var loginVC: LoginViewController?
    
    internal var isLoggedIn: Bool {
        return (FBSDKAccessToken.current() != nil)
    }
    
    func facebookLogin(completion: @escaping (_ status: Bool) -> ()) {
        let permissions = [Constant.kPublicProfileKey, Constant.kEmailKey, Constant.kUserFriendsKey, Constant.kUserPhotosKey];
        
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        
        fbLoginManager.logIn(withReadPermissions: permissions, from: loginVC) { [weak self] (result, error) -> Void in
            if error == nil, !result!.isCancelled {
                self?.returnUserData(completion: { success in
                    if (success) {
                        completion(true)
                    }
                })
                
            }
            if error != nil || (result?.isCancelled)! {
                completion(false)
            }
            
        }
    }
    
    internal func facebookLogOut(completion: @escaping (_ : Bool) -> ()) {
        FBSDKLoginManager().logOut()
        UserDefaults.standard.removeObject(forKey: Constant.userAvatar)
        UserDefaults.standard.synchronize()
        completion(true)
    }
    
   private func returnUserData(completion: @escaping (_ status: Bool) -> ()){
        if FBSDKAccessToken.current() != nil {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": Constant.kUserFBSDKGraphRequestKeys]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    guard let userInfo = result as? [String: Any] else { return }
                    if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                        UserDefaults.standard.set(imageURL, forKey: Constant.userAvatar)
                        UserDefaults.standard.synchronize()
                        completion(true)
                    }
                }
            })
        }
    }
}
