//
//  AlbumTableViewCell.swift
//  FBPhotoViewer
//
//  Created by Alex on 22.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var albumsNameLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    private var imageDownloadOperation: Cancelable?
    
    static var reuseIdentifier: String {
        return "AlbumTableViewCell"
    }
    
    var imageURL: URL? {
        didSet {
            thumbnailImageView?.image = nil
            updateUI()
        }
    }
    
    internal func configure(album: Album) {
        albumsNameLabel.text = album.name
        imageURL = URL(string: album.picture.picture.url)
    }

    private func setImage(image: UIImage) {
        self.thumbnailImageView?.image = image
        
        let screenSize =  CGSize(width: bounds.size.width / CGFloat(4) , height: (bounds.size.height - Constant.albumImageMargin))
        let imageAspectRatio = image.size.width / image.size.height
        let screenAspectRatio = screenSize.width / screenSize.height
        
        if imageAspectRatio > screenAspectRatio {
            imageWidthConstraint.constant = min(image.size.width, screenSize.width)
            imageHeightConstraint.constant = imageWidthConstraint.constant / imageAspectRatio
        }
        else {
            imageHeightConstraint.constant = min(image.size.height, screenSize.height)
            imageWidthConstraint.constant = imageHeightConstraint.constant * imageAspectRatio
        }
        self.thumbnailImageView.addCornerRadius(radius: 8.0)
        layoutIfNeeded()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageDownloadOperation?.cancel()
        imageDownloadOperation = nil
    }

    private func updateUI() {
        if let url = imageURL {
            spinner?.startAnimating()
            self.imageDownloadOperation = ImageDownloader.download(from: url, completion: { (result: UIImage?, error: Error?)  in
                if (error == nil) {
                     DispatchQueue.main.async {
                        self.imageDownloadOperation = nil
                        self.setImage(image: result!)
                        self.spinner?.stopAnimating()
                    }
                }
            })
        }
    }
}


