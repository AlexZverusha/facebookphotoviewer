//
//  PhotoCollectionViewCell.swift
//  FBPhotoViewer
//
//  Created by Alex on 22.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var photoImageView: UIImageView!
    private var imageDownloadOperation: Cancelable?
    
    static var reuseIdentifier: String {
        return "PhotoCollectionViewCell"
    }
    
    var imageURL: URL? {
        didSet {
            photoImageView?.image = nil
            updateUI()
        }
    }
    
    internal func configure(photo: Photo) {
        imageURL = URL(string: photo.thumbnail)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageDownloadOperation?.cancel()
        imageDownloadOperation = nil
    }
    
    private func updateUI() {
        if let url = imageURL {
            spinner?.startAnimating()
            self.imageDownloadOperation = ImageDownloader.download(from: url, completion: { (result: UIImage?, error: Error?)  in
                if (error == nil) {
                    DispatchQueue.main.async {
                        self.imageDownloadOperation = nil
                        self.photoImageView?.image = result
                        self.spinner?.stopAnimating()
                    }
                }
            })
        }
    }
}
