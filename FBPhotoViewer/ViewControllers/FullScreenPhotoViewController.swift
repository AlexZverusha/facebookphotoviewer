//
//  FullScreenPhotoViewController.swift
//  FBPhotoViewer
//
//  Created by Alex on 21.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

class FullScreenPhotoViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var photoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var photoWidthConstraint: NSLayoutConstraint!
    private var imageDownloadOperation: Cancelable?
        
    var imageURL: URL? {
        didSet {
            photoImageView?.image = nil
            updateImage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageDownloadOperation = nil
        spinner?.startAnimating()
        view.backgroundColor = CommonAppearance.commonBGColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imageDownloadOperation?.cancel()
        imageDownloadOperation = nil
    }
        
    @IBAction func handleSwipe() {
         dismiss(animated: true, completion: nil)
    }
        
    func setImage(image: UIImage) {
        photoImageView.image = image
        let screenSize =  CGSize(width: UIScreen.main.bounds.size.width - 16 , height: UIScreen.main.bounds.size.height)
        let imageAspectRatio = image.size.width / image.size.height
        let screenAspectRatio = screenSize.width / screenSize.height
        
        if imageAspectRatio > screenAspectRatio {
            photoWidthConstraint.constant = min(image.size.width, screenSize.width)
            photoHeightConstraint.constant = photoWidthConstraint.constant / imageAspectRatio
        }
        else {
            photoHeightConstraint.constant = min(image.size.height, screenSize.height)
            photoWidthConstraint.constant = photoHeightConstraint.constant * imageAspectRatio
        }
        
        view.layoutIfNeeded()
    }   
}

extension FullScreenPhotoViewController {
    
    private func updateImage() {
        if let url = imageURL {
            
            self.imageDownloadOperation = ImageDownloader.download(from: url, completion: { (result: UIImage?, error: Error?)  in
                if (error == nil) {
                    DispatchQueue.main.async {
                        self.imageDownloadOperation = nil
                        self.setImage(image: result!)
                        self.spinner?.stopAnimating()
                        self.imageDownloadOperation = nil
                    }
                }
            })
        }
    }
}
