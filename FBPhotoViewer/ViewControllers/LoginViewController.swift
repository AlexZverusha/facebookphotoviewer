//
//  LoginViewController.swift
//  FBPhotoViewer
//
//  Created by Alex on 21.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var fbLoginButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imageUserImageView: UIImageView!
    @IBOutlet weak var fbLogOutButton: UIButton!
    private var imageDownloadOperation: Cancelable?
    
    fileprivate let model = AuthModel()
    fileprivate var isloggedIn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imageDownloadOperation?.cancel()
        imageDownloadOperation = nil
    }
    
    private func configure() {
        statusLabel.isHidden = true
        view.backgroundColor = CommonAppearance.loginBGColor
        model.delegate = self
        model.getUIState()
    }
    
    private func showAlbumsList() {
        performSegue(withIdentifier: Constant.showAlbumsSegueID , sender: nil)
    }
    
    @IBAction func fbLoginButtonClicked(_ sender: Any) {
        if (isloggedIn) {
            showAlbumsList()
            return
        }
        spinner.startAnimating()
        fbLoginButton.isUserInteractionEnabled = false
        model.getFBLogin()
    }
    
    @IBAction func fbLogOutButtonClicked(_ sender: Any) {
        model.getFBLogOut()
        
    }
    
    fileprivate func showLogInState() {
        fbLoginButton.setTitle(Constant.fbLoginButtonTitle, for: .normal)
        fbLogOutButton.isHidden = true
        isloggedIn = false
        imageUserImageView.isHidden = true
        imageUserImageView.image = nil
        statusLabel.isHidden = true
    }
    
    fileprivate func showLoggedInState() {
        spinner.stopAnimating()
        fbLoginButton.isUserInteractionEnabled = true
        fbLoginButton.setTitle(Constant.showAlbumsButtonTitle, for: .normal)
        isloggedIn = true
        statusLabel.isHidden = false
        imageUserImageView.isHidden = false
        fbLogOutButton.isHidden = false
        guard let link = model.userAvatarLink else {return}
        showImageUserProfile(link: link)
        statusLabel.isHidden = false
    }
    
}

extension LoginViewController: AuthModelDelegate {
    
    func updateUIWith(state: AuthState) {
        switch state {
        case .loginIn:
            showLogInState()
            return
        case .loggedIn:
            showLoggedInState()
            return
        case .fail:
            spinner.stopAnimating()
            fbLoginButton.isUserInteractionEnabled = true
            displayUIAlertController(title: Constant.failMessageTitle, message: Constant.failMessageText)
        }
    }
}

extension LoginViewController {
    
    private func showImageUserProfile(link: String) {
        guard let url = URL(string: link) else {return}
        imageDownloadOperation = ImageDownloader.download(from: url, completion: {(result: UIImage?, error: Error?)   in
            if (error == nil) {
                self.imageDownloadOperation = nil
                DispatchQueue.main.async {
                self.imageUserImageView.image = result
                self.imageUserImageView.makeCircular()
                self.spinner?.stopAnimating()
                }
            }
        })
    }
}
