//
//  PhotoCollectionsViewController.swift
//  FBPhotoViewer
//
//  Created by Alex on 22.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

class PhotoCollectionsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var album: Album?
    let model = ListModel()
    var photos = [Photo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    fileprivate func config() {
        view.backgroundColor = CommonAppearance.commonBGColor
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        title = album?.name
        let nib = UINib(nibName: Constant.photoCollectionViewCellNibName, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: PhotoCollectionViewCell.reuseIdentifier)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        model.delegate = self
        guard let albumID = album?.id else {
            return
        }
        model.getPhotoList(idAlbum: albumID)
    }
    
    // MARK: - Navigation
    private func showPhotoVC(photo: Photo) {
        performSegue(withIdentifier: Constant.showPhotoSegueID, sender: photo)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == Constant.showPhotoSegueID {
            let vc = segue.destination as! FullScreenPhotoViewController
            let photo = sender as! Photo
            vc.imageURL = URL(string: photo.images.first!.source)
        }
    }
}

extension PhotoCollectionsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
        cell.configure(photo: photos[indexPath.row])
        return cell
    }
}

extension PhotoCollectionsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       showPhotoVC(photo: photos[indexPath.row])
    }
}

extension PhotoCollectionsViewController: ListModelDelegate {
    
    func updatePhotosList(photos: [Photo]) {
        self.photos = photos
        collectionView.reloadData()
    }
}
