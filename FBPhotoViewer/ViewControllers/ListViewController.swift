//
//  ListViewController.swift
//  FBPhotoViewer
//
//  Created by Alex on 21.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var albums = [Album]()
    let model = ListModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    fileprivate func config() {
        title = Constant.listViewControllerTitle
        let nib = UINib(nibName: Constant.albumTableViewCellNibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: AlbumTableViewCell.reuseIdentifier)
        model.delegate = self
        model.getAlbumsList()
        view.backgroundColor = CommonAppearance.commonBGColor
    }
}


extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AlbumTableViewCell.reuseIdentifier) as? AlbumTableViewCell
        cell?.configure(album: albums[indexPath.row])
        return cell!
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Constant.mainStoryboardName, bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: Constant.photoCollectionsVCID) as! PhotoCollectionsViewController
        vc.album = albums[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ListViewController: ListModelDelegate {
    func updatePhotosList(photos: [Photo]) {
    }
    
    func updateAlbumsList(albums:[Album]) {
        self.albums = albums
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

