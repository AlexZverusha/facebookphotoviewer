//
//  ListModel.swift
//  FBPhotoViewer
//
//  Created by Alex on 22.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import Foundation

class ListModel {
    
    weak var delegate: ListModelDelegate?
    private let photoManager = FBPhotoManager()
    
    internal func getAlbumsList() {
        photoManager.fetchAlbums(completion: {list in
            self.delegate?.updateAlbumsList(albums:list)
        })
    }
    
    internal func getPhotoList(idAlbum: String) {
        photoManager.fetchPhotos(idAlbum: idAlbum, completion: {list in
            self.delegate?.updatePhotosList(photos: list)
        })
    }
}

protocol  ListModelDelegate: class {
    func updateAlbumsList(albums:[Album])
    func updatePhotosList(photos:[Photo])
}


extension ListModelDelegate {
    
    func updateAlbumsList(albums: [Album]) {
    }
}
