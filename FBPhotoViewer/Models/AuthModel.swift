//
//  AuthModel.swift
//  FBPhotoViewer
//
//  Created by Alex on 23.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

enum AuthState {
    case loginIn
    case loggedIn
    case fail
}

class AuthModel  {
    
    private let fbLoginManager = FBLoginManager()
    weak var delegate: AuthModelDelegate?
    internal var userAvatarLink: String?
    
    internal func getUIState() {
        if (fbLoginManager.isLoggedIn) {
            userAvatarLink = UserDefaults.standard.string(forKey: Constant.userAvatar)
            delegate?.updateUIWith(state: .loggedIn)
            return
        }
        delegate?.updateUIWith(state: .loginIn)
    }
    
    internal func getFBLogOut() {
        fbLoginManager.facebookLogOut(completion: { isSuccess in
            if (isSuccess) {
                self.userAvatarLink = nil
                self.delegate?.updateUIWith(state: .loginIn)
            }
        })
    }
    
    internal func getFBLogin() {
        fbLoginManager.facebookLogin(completion: { isSuccess in
            if (isSuccess) {
                self.userAvatarLink = UserDefaults.standard.string(forKey: Constant.userAvatar)
                self.delegate?.updateUIWith(state: .loggedIn)
                return
            }
            self.delegate?.updateUIWith(state: .fail)
        })
    }
}

protocol AuthModelDelegate: class {
    func updateUIWith(state: AuthState)
}









