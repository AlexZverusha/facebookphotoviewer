//
//  Models.swift
//  FBPhotoViewer
//
//  Created by Alex on 22.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import Foundation

struct Albums: Codable {
    var albums: [Album]
    
    enum CodingKeys: String, CodingKey {
        case albums = "data"
    }
}

struct Album: Codable {
    var name: String
    var id: String
    var picture: PictureData
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case picture
    }
}

struct PictureData: Codable {
    var picture: Picture
    
    enum CodingKeys: String, CodingKey {
        case picture = "data"
    }
}

struct Picture: Codable {
    var url: String
    
    enum CodingKeys: String, CodingKey {
        case url
    }
}

struct Photos: Codable {
    var photos: [Photo]
    
    enum CodingKeys: String, CodingKey {
        case photos = "data"
    }
}

struct Photo: Codable {
    var id: String
    var link: String
    var thumbnail: String
    var images: [Images]
    
    enum CodingKeys: String, CodingKey {
        case id
        case link
        case thumbnail = "picture"
        case images
    }
}

struct Images: Codable {
    var source: String
    
    enum CodingKeys: String, CodingKey {
        case source 
    }
}
