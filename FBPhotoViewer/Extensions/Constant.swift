//
//  Constant.swift
//  FBPhotoViewer
//
//  Created by Alex on 24.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

struct Constant {
    static let userAvatar = "userAvatar"
    static let fetchAlbumsParameters = ["fields": "cover_photo, name, picture"]
    static let fetchPhotosParameters =  ["fields": "link, picture, images", "limit": "1000"]
    static let failMessageTitle = "Something wrong"
    static let failMessageText = "Try again please!"
    static let defaultBorderWidth: CGFloat = 2
    static let kPublicProfileKey = "public_profile"
    static let kEmailKey = "email"
    static let kUserFriendsKey = "user_friends"
    static let kUserPhotosKey = "user_photos"
    static let kUserFBSDKGraphRequestKeys = "id, name, first_name, last_name, picture.type(large), email"
    static let errorParsingMessage = "error parsing response"
    static let fbLoginButtonTitle = "Login Facebook"
    static let showAlbumsButtonTitle = "Let's see photos"
    static let showAlbumsSegueID = "showList"
    static let photoCollectionsVCID = "PhotoCollectionsViewController"
    static let albumTableViewCellNibName = "AlbumTableViewCell"
    static let listViewControllerTitle = "Albums"
    static let mainStoryboardName = "Main"
    static let albumImageMargin: CGFloat = 16
    static let showPhotoSegueID = "showPhoto"
    static let photoCollectionViewCellNibName = "PhotoCollectionViewCell"
}
