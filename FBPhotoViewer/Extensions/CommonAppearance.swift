//
//  CommonAppearance.swift
//  FBPhotoViewer
//
//  Created by Alex on 21.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

struct CommonAppearance {
    static let loginBGColor = #colorLiteral(red: 0.3137254902, green: 0.4039215686, blue: 0.6862745098, alpha: 1)
    static let commonBGColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
}
