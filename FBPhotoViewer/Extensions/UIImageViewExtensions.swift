//
//  UIImageViewExtensions.swift
//  FBPhotoViewer
//
//  Created by Alex on 22.11.17.
//  Copyright © 2017 Oleksandr Vashchenko. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func addBorderWithColor(_ color: UIColor) {
        layer.borderColor = color.cgColor
        layer.borderWidth = CGFloat(Constant.defaultBorderWidth)
    }
    
    func addCornerRadius(radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func makeCircular() {
        layer.cornerRadius = frame.size.width / 2
        clipsToBounds = true
    }
    
    func roundCornersForAspectFit(radius: CGFloat) {
        if let image = self.image {
            let boundsScale = self.bounds.size.width / self.bounds.size.height
            let imageScale = image.size.width / image.size.height
            
            var drawingRect: CGRect = self.bounds
            
            if boundsScale > imageScale {
                drawingRect.size.width =  drawingRect.size.height * imageScale
                drawingRect.origin.x = (self.bounds.size.width - drawingRect.size.width) / 2
            } else {
                drawingRect.size.height = drawingRect.size.width / imageScale
                drawingRect.origin.y = (self.bounds.size.height - drawingRect.size.height) / 2
            }
            let path = UIBezierPath(roundedRect: drawingRect, cornerRadius: radius)
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}

extension UIImageView {
    func render(with radius: CGFloat) {

        self.backgroundColor = UIColor.clear
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 10)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 10

        let borderView = UIView()
        borderView.frame = self.bounds
        borderView.layer.cornerRadius = radius
        borderView.layer.masksToBounds = true
        self.addSubview(borderView)

        let subImageView = UIImageView()
        subImageView.image = self.image
        subImageView.frame = borderView.bounds
        borderView.addSubview(subImageView)

        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}

extension UIViewController {
    
    func displayUIAlertController(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}




